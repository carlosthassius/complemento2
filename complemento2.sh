#!/bin/bash

l1=$(wc -l <$1)
l2=$(wc -l <$2)
l3=$(wc -l <$3)
l4=$(wc -l <$4)
[ $l1 -gt $l2 ] && [ $l1 -gt $l3 ] && [ $l1 -gt $l4 ] && echo $(cat $1)
[ $l2 -gt $l1 ] && [ $l2 -gt $l3 ] && [ $l2 -gt $l4 ] && echo $(cat $2)
[ $l3 -gt $l1 ] && [ $l3 -gt $l2 ] && [ $l3 -gt $l4 ] && echo $(cat $3)
[ $l4 -gt $l1 ] && [ $l4 -gt $l2 ] && [ $l4 -gt $l3 ] && echo $(cat $4)
